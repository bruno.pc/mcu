package model;
public class VideoModel {
	public int id, idPost;
	public String bas64, ext;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdPost() {
		return idPost;
	}
	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}
	public String getBas64() {
		return bas64;
	}
	public void setBas64(String bas64) {
		this.bas64 = bas64;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	
}
