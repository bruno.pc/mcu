package model;
public class UserModel {
	public UserModel(int id, String name, String email, String pwd, int level) {
		this.setId(id);
		this.setName(name);
		this.setEmail(email);
		this.setPwd(pwd);
		this.setLevel(level);
	}
	private int id;
	private String name;
	private String email;
	private String pwd;
	private int level;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
}
