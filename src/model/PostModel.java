package model;
public class PostModel {
	public int id;
	public String title, summary, content;

	public PostModel(int id, String title, String summary, String content) {
		this.setId(id);
		this.setTitle(title);
		this.setSummary(summary);
		this.setContent(content);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
