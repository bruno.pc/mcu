package bean;
import model.PostModel;
import model.ImageModel;
import model.VideoModel;
import control.PostControl;
import control.ImageControl;
import control.VideoControl;

public class PostBean {
	private int id, idImage, idVideo;
	private String title, summary, content, base64Image, base64Video, extImage, extVideo;
	
	public void addPost() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdImage() {
		return idImage;
	}
	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}
	public int getIdVideo() {
		return idVideo;
	}
	public void setIdVideo(int idVideo) {
		this.idVideo = idVideo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getBase64Image() {
		return base64Image;
	}
	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}
	public String getBase64Video() {
		return base64Video;
	}
	public void setBase64Video(String base64Video) {
		this.base64Video = base64Video;
	}
	public String getExtImage() {
		return extImage;
	}
	public void setExtImage(String extImage) {
		this.extImage = extImage;
	}
	public String getExtVideo() {
		return extVideo;
	}
	public void setExtVideo(String extVideo) {
		this.extVideo = extVideo;
	}
	
}
