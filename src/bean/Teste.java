package bean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
@ManagedBean(name = "testeBean")
@ViewScoped
public class Teste {
	String nome;
	String sobrenome;
	String doinho;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getDoinho() {
		return doinho;
	}

	public void setDoinho(String doinho) {
		this.doinho = doinho;
	}

	public void mopa() {
		this.doinho = this.nome + " " + this.sobrenome;
		 FacesContext.getCurrentInstance().addMessage("form", new FacesMessage("mopa"));
	}
}
