package bean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import control.UserControl;
import model.UserModel;
@ManagedBean(name = "UserBean")
@SessionScoped
public class UserBean {	
	private String name, email, pwd;
	private int id, level;
	boolean logged = false;
	
	public void addUser() throws Exception {
		UserModel user = new UserModel(this.getId(), this.getName(), this.getEmail(), this.getPwd(), this.getLevel());
		if(new UserControl().addUser(user)){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu certo"));
		}else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu merda"));
		}
	}
	
	public String login() throws Exception {
		UserModel user = new UserModel(this.getId(), this.getName(), this.getEmail(), this.getPwd(), this.getLevel());
		int result = new UserControl().login(user);
		if(result == 1) {
			this.setLogged(true);
			return "index.xhtml?faces-redirect=true";
		}
		return null;
	}
	
	public String editUser() throws Exception {
		UserModel user = new UserModel(this.getId(), this.getName(), this.getEmail(), this.getPwd(), this.getLevel());
		if(new UserControl().editUser(user)) {
			return "index";
		}else {
			return "edit";
		}
	}
	public boolean verifyUser() {
		return !this.getLogged(); 
	}
	public String logout() throws Exception {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "mopa.xhtml";
	}
	
	//Getters and Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}

	public boolean getLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}
}