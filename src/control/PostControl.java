package control;
import control.Conexao;
import model.PostModel;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class PostControl {
	public int addPost(PostModel post) {
		int id = 0;
			try {
				Connection con = new Conexao().abrirConexao();
				String sql = "INSERT INTO post(title, summary, content) VALUES(?, ?, ?);";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, post.getTitle());
				ps.setString(2, post.getSummary());
				ps.setString(3, post.getContent());
				if(!ps.execute()) {
					sql = "SELECT id FROM post ORDER BY id DESC LIMIT 1";
					ps = con.prepareStatement(sql);
					ResultSet rs = ps.executeQuery();
					if(rs.next()) {
						id = rs.getInt("id");
					}
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		return id;
	}
	public ArrayList<PostModel> allPosts(){
		ArrayList<PostModel> list = new ArrayList<PostModel>();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM post";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					PostModel post = new PostModel(rs.getInt("id"), rs.getString("title"), rs.getString("summary"), rs.getString("content"));
					list.add(post);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return list;
	}
	public boolean updatePost(PostModel post) {
		boolean result = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE post SET title = ?, summary = ?, content = ? WHERE id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, post.getTitle());
			ps.setString(2, post.getSummary());
			ps.setString(3, post.getContent());
			ps.setInt(4, post.getId());
			if(!ps.execute()) {
				result = true;
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return result;
	}
}
