package control;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.UserBean;
public class FilterClass implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		UserBean session = (UserBean) req.getSession().getAttribute("UserBean"); //MUDA ESSA LINHA - COLOCAR NOME DA BEAN Q TA FAZENDO LOGIN
		String url = req.getRequestURI();
		if(session == null || !session.getLogged()) {
			if(url.indexOf("NewFile.xhtml") >=0) { //PAGINA QUE VAI SER FILTRADA, Q S� VAI SER ACESSADA BASEADO NA SESS�O
				res.sendRedirect(req.getServletContext().getContextPath() + "/mopa.xhtml"); // P�GINA QUE VAI SER ENVIADA CASO N�O ESTEJA LOGADO
			}else {
				chain.doFilter(request, response); //N�O SEI OQ ISSO FAZ
			}
		}else {
			if(url.indexOf("login.xhtml") >=0) { //P�gina q n pode ser acessada caso a pessoa esteja logada, tipo a login
				res.sendRedirect(req.getServletContext().getContextPath() + "/index.xhtml"); //Pra onde a pessoa vai ser enviada.
			}else {
				chain.doFilter(request, response); //NOVAMENTE N SEI OQ FAZ
			}
		}
		
	}
}