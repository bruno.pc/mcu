package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.VideoModel;

public class VideoControl {
	public boolean addVideo(VideoModel video) {
		boolean result = false;
			try {
				Connection con = new Conexao().abrirConexao();
				String sql = "INSERT INTO video(base64, ext) VALUES(?, ?);";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, video.getBas64());
				ps.setString(2, video.getExt());
				if(!ps.execute()) {
					result = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		return result;
	}
	public boolean editVideo(VideoModel video) {
		boolean result = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE video SET base64 = ?, ext = ? WHERE = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, video.getBas64());
			ps.setString(2, video.getExt());
			ps.setInt(3, video.getId());
			if(!ps.execute()) {
				result = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return result;
	}
}
