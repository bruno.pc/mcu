package control;
import model.UserModel;
import control.Crypt;
import control.Conexao;
import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.nio.file.Files;
import java.nio.file.Paths;
public class UserControl {
	public boolean addUser(UserModel user) throws Exception {
		boolean result = false;
		try {
			Connection con = new Conexao().abrirConexao();
			
			Crypt crypt = new Crypt();
			String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + "/resources/ByBpSs/";
			String key = new String(Files.readAllBytes(Paths.get(path + "key.txt")));
			byte [] encPwd = crypt.encrypt(user.getPwd(), key);
			String sql = "INSERT INTO user(name, email, pwd, level) VALUES(?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getName());
			ps.setString(2, user.getEmail());
			ps.setBytes(3, encPwd);
			ps.setInt(4, user.getLevel());
			if(!ps.execute()) {
				result = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			result = false;
		}
		return result;
	}
	public int login(UserModel userVerify) throws Exception {
		int result = 0;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT id, pwd, count(id) as c FROM user WHERE email = ?";
			String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + "/resources/ByBpSs/";
			String key = new String(Files.readAllBytes(Paths.get(path + "key.txt")));
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, userVerify.getEmail());
			ResultSet rs = ps.executeQuery();
			if(rs!=null) {
				rs.next();
				byte [] encPwd = rs.getBytes("pwd");
				Crypt crypt = new Crypt();
				String pwd = crypt.decrypt(encPwd, key);
				if(pwd.equals(userVerify.getPwd())) {
					result = 1;
				}else {
					result = 3;
				}
			}else {
				result = 2;
			}
			new Conexao().fecharConexao(con);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return result;
	}
	public boolean editUser(UserModel user) throws Exception {
		boolean result = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE user SET name = ?, pwd = ? WHERE id= ?";
			
			Crypt crypt = new Crypt();
			String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + "/resources/ByBpSs/";
			String key = new String(Files.readAllBytes(Paths.get(path + "key.txt")));
			byte [] encPwd = crypt.encrypt(user.getPwd(), key);
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getName());
			ps.setBytes(2, encPwd);
			ps.setInt(3, user.getId());
			if(!ps.execute()) {
				result = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.print(e.getMessage());
		}
		return result;
	}
}